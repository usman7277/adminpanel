<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('pages.home');});
Route::get('/users-profile', function () {return view('pages.user_profile');});
Route::get('/faq', function () {return view('pages.faq');});
Route::get('/contact', function () {return view('pages.contact');});
Route::get('/login', function () {return view('pages.login');});
Route::get('/register', function () {return view('pages.register');});
Route::get('/404', function () {return view('pages.error404');});
